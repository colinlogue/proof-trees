module Inference.System exposing (..)

import AssocSet exposing (Set)
import Formula exposing (UnificationResult)
import GenericDict exposing (Dict)
import Inference.Rule exposing (Rule)


type alias System ruleType formula prop =
    { rule : ruleType -> Rule formula
    , name : ruleType -> String
    , label : ruleType -> String
    , printFormula : formula -> String
    , allRules : List ruleType
    , same : ruleType -> ruleType -> Bool
    , unify : formula -> formula -> UnificationResult prop formula
    , substitute : Dict prop formula -> formula -> Maybe formula
    , compareProp : prop -> prop -> Bool
    , compareFormula : formula -> formula -> Bool
    , printProp : prop -> String
    , getProps : formula -> Set prop
    , readProp : String -> Maybe prop
    , makeFormula : prop -> formula
    }
