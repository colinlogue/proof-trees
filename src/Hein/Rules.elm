module Hein.Rules exposing
    ( HeinRule(..)
    , Side(..)
    , rule
    , false
    , label
    , system
    )

import AssocSet as Set
import Formula exposing (Formula(..), and, arrow, neg, or)
import Inference.Rule exposing (Rule, from, fromProof, infer)
import Inference.System exposing (System)


system : System HeinRule (Formula Proposition) Proposition
system =
    { name = fullName
    , rule = rule
    , label = label
    , printFormula = Formula.print identity
    , allRules = allRules
    , same = \r1 r2 -> fullName r1 == fullName r2
    , unify = Formula.unify identity (==)
    , compareProp = (==)
    , compareFormula = Formula.compare (==)
    , printProp = identity
    , substitute = Formula.substitute (==) identity
    , getProps = Formula.atoms >> Set.fromList
    , readProp = Just
    , makeFormula = Variable
    }

type alias Proposition = String

a = Variable "A"
b = Variable "B"

type Side
    = Left
    | Right

type HeinRule
    = Conjunction
    | Simplification Side
    | Addition Side
    | DisjunctiveSyllogism Side
    | ModusPonens
    | ConditionalProof
    | DoubleNegation Side
    | Contradiction
    | IndirectProof

allRules : List HeinRule
allRules =
    [ Conjunction
    , Simplification Left
    , Simplification Right
    , Addition Left
    , Addition Right
    , DisjunctiveSyllogism Left
    , DisjunctiveSyllogism Right
    , ModusPonens
    , ConditionalProof
    , DoubleNegation Left
    , DoubleNegation Right
    , Contradiction
    , IndirectProof
    ]

false : Formula Proposition
false = Variable "False"

rule : HeinRule -> Rule (Formula Proposition)
rule hein =
    case hein of
        Conjunction ->
            from [a, b]
                |> infer (and a b)

        Simplification Left ->
            from [and a b]
                |> infer a

        Simplification Right ->
            from [and a b]
                |> infer b

        Addition Left ->
            from [a]
                |> infer (or a b)

        Addition Right ->
            from [a]
                |> infer (or b a)

        DisjunctiveSyllogism Left ->
            from [or a b, neg a]
                |> infer b

        DisjunctiveSyllogism Right ->
            from [or a b, neg b]
                |> infer a

        ModusPonens ->
            from [a, arrow a b]
                |> infer b

        ConditionalProof ->
            fromProof [a] b
                |> infer (arrow a b)

        DoubleNegation Left ->
            from [neg (neg a)]
                |> infer a

        DoubleNegation Right ->
            from [a]
                |> infer (neg (neg a))

        Contradiction ->
            from [a, neg a]
                |> infer false

        IndirectProof ->
            fromProof [neg a] false
                |> infer a

fullName : HeinRule -> String
fullName hein =
    let
        sideName side =
            case side of
                Left -> " (left)"
                Right -> " (right)"
    in
    case hein of
        Conjunction ->
            "Conjunction"

        Simplification side ->
            "Simplification" ++ sideName side

        Addition side ->
            "Addition" ++ sideName side

        DisjunctiveSyllogism side ->
            "Disjunctive Syllogism" ++ sideName side

        ModusPonens ->
            "Modus Ponens"

        ConditionalProof ->
            "Conditional Proof"

        DoubleNegation Left ->
            "Double Negation (remove)"

        DoubleNegation Right ->
            "Double Negation (add)"

        Contradiction ->
            "Contradiction"

        IndirectProof ->
            "Indirect Proof"

label : HeinRule -> String
label hein =
    case hein of
        Conjunction ->
            "Conj"

        Simplification Left ->
            "SimpL"

        Simplification Right ->
            "SimpR"

        Addition Left ->
            "AddL"

        Addition Right ->
            "AddR"

        DisjunctiveSyllogism Left ->
            "DSL"

        DisjunctiveSyllogism Right ->
            "DSR"

        ModusPonens ->
            "MP"

        ConditionalProof ->
            "CP"

        DoubleNegation Left ->
            "DN-"

        DoubleNegation Right ->
            "DN+"

        Contradiction ->
            "Contr"

        IndirectProof ->
            "IP"
