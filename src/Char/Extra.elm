module Char.Extra exposing (..)


nextAlpha : List Char -> List Char
nextAlpha chars =
    let
        incChar : Char -> (Char, Bool)
        incChar c =
            if Char.toCode c < Char.toCode 'A'
            then ('A', False)
            else if Char.toCode c >= Char.toCode 'Z'
                then ('A', True)
                else (Char.toCode c + 1 |> Char.fromCode, False)
        recNextAlpha : List Char -> (List Char, Bool)
        recNextAlpha css =
            case css of
                [] ->
                    ([], True)

                c::cs ->
                    case recNextAlpha cs of
                        (newCs, True) ->
                            case incChar c of
                                (newC, overflow) ->
                                    (newC::newCs, overflow)
                        (newCs, False) ->
                            (c::newCs, False)
    in
    case recNextAlpha chars of
        (cs, True) ->
            'A' :: cs

        (cs, False) ->
            cs
