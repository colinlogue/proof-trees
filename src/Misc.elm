module Misc exposing (..)



enumerate : List a -> List (Int, a)
enumerate =
    let
        implementation : Int -> List a -> List (Int, a)
        implementation i list =
            case list of
                [] ->
                    []

                x::xs ->
                    (i,x)::implementation (i+1) xs
    in
    implementation 0

