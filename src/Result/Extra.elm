module Result.Extra exposing (..)


toMaybeErr : Result err ok -> Maybe err
toMaybeErr result =
    case result of
        Ok _ ->
            Nothing

        Err err ->
            Just err


combine : (err -> err -> err) -> err -> List (Result err ok) -> Result err (List ok)
combine concat empty resultList =
    case List.filterMap toMaybeErr resultList of
        [] ->
            Ok <| List.filterMap Result.toMaybe resultList

        errs ->
            Err <| List.foldr concat empty errs