module Inference.Proof exposing
    (Proof(..)
    , instantiateRule
    , substitute
    , assumptions
    , names
    )

import AssocSet as Set exposing (Set)
import Inference.Rule as Rule exposing (Rule)
import Maybe.Extra exposing (combine)
import Result.Extra
import Sequent exposing (Sequent)


type Proof a =
    Proof
        { rule : Rule a
        , conclusion : a
        , subproofs : List (Proof a)
        , discharges : Set a
        }


instantiateRule : (formula -> Maybe formula) -> Rule formula -> Result String (Proof formula)
instantiateRule sub rule =
    let
        resultSubproofs =
            Maybe.map (Result.Extra.combine (++) "")
                <| combine
                <| List.map (Maybe.map (Rule.assume >> instantiateRule sub))
                <| List.map (.consequent >> sub) rule.conditions
        maybeConclusion = sub rule.conclusion
        maybeDischarges =
            Maybe.map (Set.fromList)
                <| combine
                <| List.foldr (++) []
                <| List.map (.antecedents >> List.map sub) rule.conditions
    in
    case (resultSubproofs, maybeConclusion, maybeDischarges) of
        (Just (Ok subproofs), Just conclusion, Just discharges) ->
            Proof
                { rule = rule
                , conclusion = conclusion
                , subproofs = subproofs
                , discharges = discharges
                } |> substitute sub

        _ ->
            Err "unable to instantiate rule"


substitute : (formula -> Maybe formula) -> Proof formula -> Result String (Proof formula)
substitute sub (Proof proof) =
    let
        maybeConclusion = sub proof.conclusion
        maybeSubproofs = Result.Extra.combine (++) "" <| List.map (substitute sub) proof.subproofs
        maybeDischarges = Maybe.map (Set.fromList) <| combine <| List.map sub (Set.toList proof.discharges)

    in
    case (maybeConclusion, maybeSubproofs, maybeDischarges) of
        (Just conclusion, Ok subproofs, Just discharges) ->
            Ok <| Proof { proof | conclusion = conclusion, subproofs = subproofs, discharges = discharges }

        _ ->
            Err "substitution failed"


assumptions : Proof a -> Set a
assumptions (Proof {conclusion, subproofs, discharges}) =
    if
        List.isEmpty subproofs
    then
        Set.singleton conclusion
    else
        Set.diff
            (List.foldr (assumptions >> Set.union) Set.empty subproofs)
            discharges


names : (a -> Set b) -> Proof a -> Set b
names getNames (Proof {conclusion, subproofs}) =
    let
        subnames = List.map (names getNames) subproofs
    in
    List.foldr Set.union (getNames conclusion) subnames