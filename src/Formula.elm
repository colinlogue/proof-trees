module Formula exposing (..)


import GenericDict as Dict exposing (Dict)
import Parser exposing ((|=), Parser)
import Substitution


type Formula prop
    = Conjunction (Formula prop) (Formula prop)
    | Disjunction (Formula prop) (Formula prop)
    | Implication (Formula prop) (Formula prop)
    | Negation (Formula prop)
    | Variable prop


and = Conjunction
or = Disjunction
arrow = Implication
neg = Negation
var = Variable


print : (prop -> String) -> Formula prop -> String
print printProp formula =
    let
        printBinaryOp : String -> Formula prop -> Formula prop -> String
        printBinaryOp symbol lhs rhs =
            printSubformula lhs ++ " " ++ symbol ++ " " ++ printSubformula rhs

        printSubformula : Formula prop -> String
        printSubformula subformula =
            case subformula of
                Variable _ ->
                    print printProp subformula

                Negation _ ->
                    print printProp subformula

                _ ->
                    "(" ++ print printProp subformula ++ ")"
    in
    case formula of
        Conjunction lhs rhs ->
            printBinaryOp "∧" lhs rhs

        Disjunction lhs rhs ->
            printBinaryOp "∨" lhs rhs

        Implication lhs rhs ->
            printBinaryOp "→" lhs rhs

        Variable prop ->
            printProp prop

        Negation subformula ->
            "¬" ++ printSubformula subformula



map : (a -> b) -> Formula a -> Formula b
map f formula =
    let
        binaryMap : (Formula b -> Formula b -> Formula b) -> Formula a -> Formula a -> Formula b
        binaryMap constructor lhs rhs = constructor (map f lhs) (map f rhs)

        unaryMap : (Formula b -> Formula b) -> Formula a -> Formula b
        unaryMap constructor subformula = constructor (map f subformula)
    in
    case formula of
        Conjunction lhs rhs ->
            binaryMap Conjunction lhs rhs

        Disjunction lhs rhs ->
            binaryMap Disjunction lhs rhs

        Implication lhs rhs ->
            binaryMap Implication lhs rhs

        Negation subformula ->
            unaryMap Negation subformula

        Variable x ->
            Variable <| f x

-- folds over the tree structure - should probably be replaced by atoms
fold : (a -> a -> a) -> Formula a -> a
fold f formula =
    case formula of
        Conjunction lhs rhs ->
            f (fold f lhs) (fold f rhs)

        Disjunction lhs rhs ->
            f (fold f lhs) (fold f rhs)

        Implication lhs rhs ->
            f (fold f lhs) (fold f rhs)

        Negation subformula ->
            fold f subformula

        Variable x ->
            x


atoms : Formula a -> List a
atoms formula = fold (++) <| map (List.singleton) formula


contains : (a -> a -> Bool) -> a -> Formula a -> Bool
contains compareProp x = atoms >> List.any (compareProp x)


rename : (a -> a -> Bool) -> a -> a -> Formula a -> Formula a
rename compareProp old new = map (\x -> if compareProp x old then new else x)


substitute : (a -> a -> Bool) -> (a -> String) -> Dict a (Formula a) -> Formula a -> Maybe (Formula a)
substitute compareProp printProp sub formula =
    --if
    --    List.any (\x -> contains compareProp x formula) (Dict.keys sub)
    --then
    --    Nothing
    --else
        let
            recSub = substitute compareProp printProp sub

            binarySub : (Formula a -> Formula a -> Formula a) -> Formula a -> Formula a -> Maybe (Formula a)
            binarySub constructor lhs rhs =
                case (recSub lhs, recSub rhs) of
                    (Just left, Just right) ->
                        Just <| constructor left right

                    _ ->
                        Nothing
        in
        case formula of
            Variable x ->
                case Dict.get printProp x sub of
                    Just val ->
                        Just val
                    Nothing ->
                        Just <| Variable x

            Conjunction lhs rhs ->
                binarySub Conjunction lhs rhs

            Disjunction lhs rhs ->
                binarySub Disjunction lhs rhs

            Implication lhs rhs ->
                binarySub Implication lhs rhs

            Negation subformula ->
                case recSub subformula of
                    Just val ->
                        Just <| Negation val

                    Nothing ->
                        Nothing

    --
    --case Dict.toList sub of
    --    [] ->
    --        Just formula
    --
    --    (name, val)::rest ->
    --        if contains compareProp name val
    --        then Nothing -- can't sub if name is contained in val
    --        else
    --            let
    --                recSub x = substitute compareProp printProp (Dict.fromList printProp rest) x
    --
    --                binarySub : (Formula a -> Formula a -> Formula a) -> Formula a -> Formula a -> Maybe (Formula a)
    --                binarySub constructor lhs rhs =
    --                    case (recSub lhs, recSub rhs) of
    --                        (Just left, Just right) ->
    --                            Just <| constructor left right
    --
    --                        _ ->
    --                            Nothing
    --            in
    --            case formula of
    --                Variable x ->
    --                    if compareProp name x
    --                    then Just val
    --                    else Just formula
    --
    --                Conjunction lhs rhs ->
    --                    binarySub Conjunction lhs rhs
    --
    --                Disjunction lhs rhs ->
    --                    binarySub Disjunction lhs rhs
    --
    --                Implication lhs rhs ->
    --                    binarySub Implication lhs rhs
    --
    --                Negation subformula ->
    --                    case recSub subformula of
    --                        Just x ->
    --                            Just <| Negation x
    --
    --                        Nothing ->
    --                            Nothing


type alias Subs a = Dict a (Formula a)
type alias UnificationError = List String
type alias UnificationResult prop formula = Result UnificationError (Dict prop formula)


getBinary : Formula a -> Maybe ((Formula a -> Formula a -> Formula a), Formula a, Formula a)
getBinary formula =
    case formula of
        Conjunction lhs rhs ->
            Just (Conjunction, lhs, rhs)

        Disjunction lhs rhs ->
            Just (Disjunction, lhs, rhs)

        Implication lhs rhs ->
            Just (Implication, lhs, rhs)

        _ ->
            Nothing


sameType : Formula a -> Formula a -> Bool
sameType lhs rhs =
    case (lhs, rhs) of
        (Variable _, Variable _) ->
            True

        (Negation _, Negation _) ->
            True

        (Conjunction _ _, Conjunction _ _) ->
            True

        (Disjunction _ _, Disjunction _ _) ->
            True

        (Implication _ _, Implication _ _) ->
            True

        _ ->
            False


compare : (a -> a -> Bool) -> Formula a -> Formula a -> Bool
compare compareProp lhs rhs =
    case (lhs, rhs) of
        (Variable x, Variable y) ->
            compareProp x y

        (Negation x, Negation y) ->
            compare compareProp x y

        (Conjunction xLhs xRhs, Conjunction yLhs yRhs) ->
            compare compareProp xLhs yLhs && compare compareProp xRhs yRhs

        (Disjunction xLhs xRhs, Disjunction yLhs yRhs) ->
            compare compareProp xLhs yLhs && compare compareProp xRhs yRhs

        (Implication xLhs xRhs, Implication yLhs yRhs) ->
            compare compareProp xLhs yLhs && compare compareProp xRhs yRhs

        _ ->
            False


unify : (a -> String) -> (a -> a -> Bool) -> Formula a -> Formula a -> UnificationResult a (Formula a)
unify printProp compareProp lhs rhs =
    let
        mergeSubs = Substitution.mergeSubs compareProp (compare compareProp) printProp

        binaryUnify : (Formula a -> Formula a -> Formula a)
            -> (Formula a, Formula a)
            -> (Formula a, Formula a)
            -> UnificationResult a (Formula a)
        binaryUnify constructor (xLhs, xRhs) (yLhs, yRhs) =
            case (unify printProp compareProp xLhs xRhs, unify printProp compareProp yLhs yRhs) of
                (Ok xSubs, Ok ySubs) ->
                    case mergeSubs xSubs ySubs of
                        Just subs ->
                            Ok subs

                        Nothing ->
                            Err <| List.singleton "Unable to unify"

                (x, y) ->
                    let
                        getErr z =
                            case z of
                                Err err -> err
                                _ -> []
                    in
                    Err (getErr x ++ getErr y)
    in
    case (lhs, rhs) of
        (Variable x, Variable y) ->
            if compareProp x y
            then Ok Dict.empty
            else Ok <| Dict.singleton printProp x (Variable y)

        (Variable x, y) ->
            if contains compareProp x y
            then Err
                <| List.singleton
                <| "cannot substitute " ++ printProp x ++ " into formula " ++ print printProp y
            else Ok <| Dict.singleton printProp x y

        (x, Variable y) ->
            if contains compareProp y x
            then Err
                <| List.singleton
                <| "cannot substitute " ++ printProp y ++ " into formula " ++ print printProp x
            else Ok <| Dict.singleton printProp y x

        (Conjunction xLhs xRhs, Conjunction yLhs yRhs) ->
            binaryUnify Conjunction (xLhs, xRhs) (yLhs, yRhs)

        (Disjunction xLhs xRhs, Disjunction yLhs yRhs) ->
            binaryUnify Disjunction (xLhs, xRhs) (yLhs, yRhs)

        (Implication xLhs xRhs, Implication yLhs yRhs) ->
            binaryUnify Implication (xLhs, xRhs) (yLhs, yRhs)

        (Negation x, Negation y) ->
            unify printProp compareProp x y

        _ ->
            Err <| List.singleton "unable to unify"

