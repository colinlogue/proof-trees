module Sequent exposing
    ( Sequent
    , print
    , parse
    , parser
    , assert
    , isAssertion
    , substitute
    , names
    )

import AssocSet as Set exposing (Set)
import Maybe.Extra
import Parser exposing (Parser, (|=), (|.))


type alias Sequent formula =
    { antecedents : List formula
    , consequent : formula
    }


assert : formula -> Sequent formula
assert x =
    { antecedents = []
    , consequent = x
    }


isAssertion : Sequent formula -> Bool
isAssertion {antecedents} = List.isEmpty antecedents


print : (formula -> String) -> Sequent formula -> String
print printFormula {antecedents, consequent} =
    let
        printAntecedents : String
        printAntecedents =
            List.map printFormula antecedents
                |> List.intersperse ", "
                |> List.foldr (++) ""
    in
    printAntecedents ++ " ⊢ " ++ printFormula consequent


parseAntecedents : Parser formula -> Parser (List formula)
parseAntecedents formulaParser =
    Parser.sequence
        { start = ""
        , separator = ","
        , end = ""
        , spaces = Parser.spaces
        , item = formulaParser
        , trailing = Parser.Optional
        }


parser : Parser formula -> Parser (Sequent formula)
parser formulaParser =
    Parser.succeed Sequent
        |= parseAntecedents formulaParser
        |. Parser.spaces
        |. Parser.symbol "⊢"
        |. Parser.spaces
        |= formulaParser


parse : Parser formula -> String -> Result (List Parser.DeadEnd) (Sequent formula)
parse formulaParser str =
    Parser.run
        ( Parser.succeed identity
            |= parser formulaParser
            |. Parser.end
        ) str

substitute : (formula -> Maybe formula) -> Sequent formula -> Maybe (Sequent formula)
substitute sub formula =
    let
        maybeAntecedents = Maybe.Extra.combine <| List.map sub formula.antecedents
        maybeConsequent = sub formula.consequent
    in
    case (maybeAntecedents, maybeConsequent) of
        (Just antecedents, Just consequent) ->
            Just { antecedents = antecedents, consequent = consequent }

        _ ->
            Nothing


names : (a -> Set b) -> Sequent a -> Set b
names getNames {antecedents, consequent} =
    let antecedentNames = List.map getNames antecedents
    in List.foldr Set.union (getNames consequent) antecedentNames