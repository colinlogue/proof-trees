module Main exposing (..)

import Browser
import Element
import Formula exposing (Formula)
import GenericDict as Dict
import Hein.Rules
import Interface exposing (Selection(..))
import Styles


type alias Model = Interface.Model Hein.Rules.HeinRule (Formula String) String

type alias Msg = Interface.Msg Hein.Rules.HeinRule


main : Program () Model Msg
main = Browser.document
    { init = always (
        { proofs = []
        , system = Hein.Rules.system
        , selection = NoSelection
        , messages = []
        , overNode = Nothing
        , fromBox = ""
        , toBox = ""
        , ruleSubs = Dict.empty
        }, Cmd.none)
    , view = Interface.view >> Element.layout Styles.defaultLayout >> \x ->
        { title = "Proof trees"
        , body = [x]
        }
    , update = \msg model -> Interface.update msg model |> \x -> Tuple.pair x Cmd.none
    , subscriptions = always Sub.none
    }