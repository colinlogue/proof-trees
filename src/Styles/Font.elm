module Styles.Font exposing (..)

import Element.Font as Font
import Styles.Color


family = Font.family [Font.typeface "Jost", Font.sansSerif]
color = Font.color Styles.Color.primaryText