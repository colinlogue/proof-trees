module Inference.Rule exposing (..)

import AssocSet as Set exposing (Set)
import Sequent exposing (Sequent)


type alias Rule a =
    { conditions : List (Sequent a)
    , conclusion : a
    }

assume : a -> Rule a
assume x =
    { conditions = []
    , conclusion = x
    }


from : List (a) -> (a -> Rule a)
from = List.map (\x -> { antecedents = [], consequent = x}) >> makeRule


infer : a -> (a -> Rule a) -> Rule a
infer = (|>)


makeRule : List (Sequent a) -> a -> Rule a
makeRule conditions conclusion =
    { conditions = conditions
    , conclusion = conclusion
    }


fromProof : List (a) -> a -> (a -> Rule a)
fromProof assumptions conclusion =
    \x ->
        { conditions =
            [
                { antecedents = assumptions
                , consequent = conclusion
                }
            ]
        , conclusion = x
        }

names : (a -> Set b) -> Rule a -> Set b
names getNames {conditions, conclusion} =
    let conditionsNames = List.map (Sequent.names getNames) conditions
    in List.foldr Set.union (getNames conclusion) conditionsNames