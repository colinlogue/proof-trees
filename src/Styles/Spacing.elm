module Styles.Spacing exposing (..)

import Element exposing (spacing)
import Styles.Size as Size

noSpace  = spacing 0
xxxClose = spacing Size.xxxSmall
xxClose  = spacing Size.xxSmall
xClose   = spacing Size.xSmall
close    = spacing Size.small
medClose = spacing Size.medSmall
normal   = spacing Size.medium
medWide  = spacing Size.medLarge
wide     = spacing Size.large
xWide    = spacing Size.xLarge
xxWide   = spacing Size.xxLarge
xxxWide  = spacing Size.xxxLarge