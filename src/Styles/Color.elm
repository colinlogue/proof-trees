module Styles.Color exposing (..)

import Element exposing (Color, rgb, rgb255)


primaryText : Color
primaryText = olivine

primaryBackground : Color
primaryBackground = darkPurple



-- palette generated from https://coolors.co/381d2a-3e6990-aabd8c-e9e3b4-f39b6d
darkPurple = rgb255 56 29 42
queenBlue = rgb255 62 105 144
olivine = rgb255 170 189 140
paleSpringBud = rgb255 233 227 180
atomicTangerine = rgb255 243 155 109