module Bool.Extra exposing (..)


all : List Bool -> Bool
all = List.foldr (&&) True


any : List Bool -> Bool
any = List.foldr (||) False