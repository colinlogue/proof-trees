module Substitution exposing (..)

import GenericDict as Dict exposing (Dict)


mergeSubs : (a -> a -> Bool) -> (b -> b -> Bool) -> (a -> String) -> Dict a b -> Dict a b -> Maybe (Dict a b)
mergeSubs compareProp compare printProp subsX subsY =
    case Dict.toList subsX of
        [] ->
            Just subsY

        ((k,v)::restX) ->
            case Dict.get printProp k subsY of
                Just other ->
                    if compare v other
                    then mergeSubs compareProp compare printProp (Dict.fromList printProp restX) subsY
                    else Nothing
                Nothing ->
                    mergeSubs compareProp compare printProp (Dict.fromList printProp restX) (Dict.insert printProp k v subsY)