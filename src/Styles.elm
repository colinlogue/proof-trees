module Styles exposing (..)


import Element exposing (fill, height, rgb, width)
import Element.Background as Background
import Styles.Color as Color
import Styles.Font as Font


zeros =
    { bottom = 0
    , left = 0
    , right = 0
    , top = 0
    }


defaultLayout =
    [ Font.family
    , Font.color
    , width fill
    , height fill
    , Background.color Color.primaryBackground
    ]