module Styles.Size exposing (..)


fontSize = 16

fontScale : Float -> Int
fontScale x = floor <| fontSize * x


base = toFloat (fontScale 1)
growth = 0.25

getSize : Float -> Int
getSize x = ceiling <| base ^ (1 + (growth * x))

xxxSmall = getSize -2.5
xxSmall  = getSize -2
xSmall   = getSize -1.5
small    = getSize -1
medSmall = getSize -0.5
medium   = getSize 0
medLarge = getSize 0.5
large    = getSize 1
xLarge   = getSize 1.5
xxLarge  = getSize 2
xxxLarge = getSize 2.5