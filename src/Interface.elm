module Interface exposing (..)


import Array
import AssocSet as Set exposing (Set)
import Char.Extra
import Element exposing (Attribute, Element, alignBottom, alignTop, centerX, centerY, column, el, fill, height, inFront, minimum, mouseOver, padding, paragraph, pointer, px, row, shrink, textColumn, width, wrappedRow)
import Element.Border as Border
import Element.Events exposing (onClick, onMouseEnter, onMouseLeave)
import Element.Font as Font
import Element.Input as Input
import GenericDict as Dict exposing (Dict)
import Inference.Rule as Rule exposing (Rule)
import Inference.System exposing (System)
import Inference.Proof as Proof exposing (Proof(..), instantiateRule)
import Styles exposing (zeros)
import Styles.Color as Color
import Styles.Size as Size
import Styles.Spacing as Spacing


type alias Model ruleset formula prop =
    { system : System ruleset formula prop
    , proofs : List (ProofTree formula)
    , selection : Selection ruleset
    , messages : List Message
    , overNode : Maybe NodePath
    , fromBox : String
    , toBox : String
    , ruleSubs : Dict prop formula
    }

type Msg ruleset
    = NoOp
    | SetRule ruleset
    | ClickedWorkspace
    | EnterNode NodePath
    | ExitNode NodePath
    | UpdateFromBox String
    | UpdateToBox String
    | RenameProps

type alias Position =
    { x : Float
    , y : Float
    }

type alias ProofTree formula =
    { proof : Proof formula
    --, label : String
    }

type alias Message = String

type Selection ruleset
    = Selected ruleset
    | NoSelection

type alias NodePath = List Int

assumptionStyle = [padding Size.xxSmall, Border.width 1]


nodeStyle : NodePath -> Selection ruleset -> List (Attribute (Msg ruleset))
nodeStyle path selection =
    [ onMouseEnter <| EnterNode path
    , onMouseLeave <| ExitNode path
    ] ++
    case selection of
        Selected _ ->
            [ mouseOver [(Border.glow Color.primaryText 1)] ]
        NoSelection ->
            []


renderProof : Model ruleset formula prop -> NodePath -> Proof formula -> Element (Msg ruleset)
renderProof model path (Proof {subproofs, conclusion, discharges}) =
    let
        printFormula = model.system.printFormula
        selection = model.selection
    in
    column []
        [ if List.isEmpty subproofs && Set.isEmpty discharges
            then Element.none
            else el
                [ Border.widthEach <| { zeros | bottom = 1}
                , padding Size.xxxSmall
                , width fill
                ]
                <| row
                    [ centerX
                    , Spacing.medClose
                    ]
                <|(List.map (el assumptionStyle << Element.text << printFormula) (Set.toList discharges))
                    ++ Tuple.second
                    -- this function associated each subproofs with an index in order to build
                    -- a unique path to that node
                    (List.foldl
                        (\cur (i, acc) ->
                            (i+1, acc ++ [renderProof model (path ++ [i]) cur]))
                        (Set.size discharges, [])
                        subproofs)
        , el ((nodeStyle path selection) ++ [centerX, padding Size.xxSmall])
            <| Element.text <| printFormula conclusion
        ]


renderRule : Model ruleset formula prop -> Rule formula -> Element (Msg ruleset)
renderRule model rule =
    let sub = model.system.substitute model.ruleSubs
    in
    case Proof.instantiateRule sub rule of
        Ok instance ->
            renderProof { model | selection = NoSelection } [] instance
        _ ->
            Element.none


hasSelection : Model ruleset formula prop -> Bool
hasSelection model =
    case model.selection of
        Selected _ ->
            True

        NoSelection ->
            False


isActive : Model ruleset formula prop -> ruleset -> Bool
isActive model rule =
    case model.selection of
        Selected selected ->
            model.system.same rule selected

        NoSelection ->
            False


renderSelectionBox : Model ruleset formula prop -> Element (Msg ruleset)
renderSelectionBox model =
    let
        renderBox : ruleset -> Element (Msg ruleset)
        renderBox r =
            el
                ([ pointer
                , onClick (SetRule r)
                , mouseOver
                    [ Border.glow Color.primaryText 1
                    ]
                ] ++ if isActive model r
                    then [Border.glow Color.primaryText 1]
                    else [])
                <| row [Spacing.xClose, Font.size Size.medium, padding Size.small]
                    [ renderRule model (model.system.rule r)
                    , el [centerY] <| Element.text <| model.system.label r
                    ]
    in
    column [alignBottom]
        [ el [centerX] <| Element.text "Rules"
        , wrappedRow [Spacing.close]
            <| List.map renderBox model.system.allRules
        ]


renderPreview : Model ruleset formula prop -> Element (Msg ruleset)
renderPreview model =
    case model.selection of
        Selected rule ->
            renderRule model (model.system.rule rule)

        NoSelection ->
            Element.none


renderRenameBox : Model ruleset formula prop -> Element (Msg ruleset)
renderRenameBox model =
    row
        []
        [ Input.text []
            { onChange = UpdateFromBox
            , text = model.fromBox
            , placeholder = Nothing
            , label = Input.labelHidden "from"
            }
        , Element.text " -> "
        , Input.text []
            { onChange = UpdateToBox
            , text = model.toBox
            , placeholder = Nothing
            , label = Input.labelHidden "to"
            }
        , Input.button []
            { onPress = Just RenameProps
            , label = Element.text "Go"
            }
        ]

renderMessages : List Message -> Element (Msg ruleset)
renderMessages messages =
    paragraph
        [ Spacing.normal
        ]
            <| List.map Element.text messages


renderSidebar : Model ruleset formula prop -> Element (Msg ruleset)
renderSidebar model =
    column
        [ width <| px 300
        , alignTop
        , padding Size.medium
        ]
        [ Element.text "Rename propositions in rules"
        , renderRenameBox model
        , Element.text "Messages"
        , renderMessages model.messages
        ]


renderWorkspace : Model ruleset formula prop -> Element (Msg ruleset)
renderWorkspace model =
    column
        ([ width fill
        , height (minimum 300 fill)
        , onClick ClickedWorkspace
        ] ++ if hasSelection model
            then [pointer]
            else [])
        [ row
            [ centerX
            , centerY
            , Spacing.wide
            ]
            <| Tuple.second
            <| List.foldr
                (\cur (i, acc) ->
                    (i+1, acc ++ [renderProof model [i] cur.proof]))
                (0,[])
                model.proofs
        ]
        --, renderPreview model
        --]


view : Model ruleset formula prop -> Element (Msg ruleset)
view model =
    column
        [ width fill
        , height fill
        ]
        [ row
            [ width fill
            , height fill
            ]
            [ renderWorkspace model
            , renderSidebar model ]
        , renderSelectionBox model
        ]


getNextName : Set String -> String -> String
getNextName used str =
    let next = Char.Extra.nextAlpha (String.toList str) |> String.fromList
    in
    if Set.member str used
    then getNextName used next
    else str


updateProof : NodePath -> Model ruleset formula prop -> Rule formula -> Proof formula -> Result Message (Proof formula)
updateProof path model rule (Proof current) =
    let
        {system} = model
        {compareProp, compareFormula, printProp, unify, substitute, getProps} = system
    in
    case path of
        [] ->
            case Debug.log "unify" <| unify current.conclusion rule.conclusion of
                Ok subs ->
                    instantiateRule (substitute subs) rule


                Err error ->
                    Err <| String.concat (List.intersperse "\n" error)

        x::xs ->
            let
                subproofArray = Debug.log "subproofArray" <| Array.fromList current.subproofs
            in
            case Debug.log "get x" <| Array.get x <| subproofArray of
                Just subproof ->
                    case updateProof xs model rule subproof of
                        Ok newProof ->
                            Ok <|
                                Proof
                                    { current
                                        | subproofs = Debug.log "set x"
                                            (Array.set x newProof subproofArray |> Array.toList)
                                    }
                        Err err ->
                            Err err

                Nothing ->
                    Err "invalid path to node"


update : (Msg ruleset) -> Model ruleset formula prop -> Model ruleset formula prop
update msg model =
    case msg of
        NoOp ->
            model

        SetRule rule ->
            { model | selection = Selected rule }

        ClickedWorkspace ->
            case (model.overNode, model.selection) of
                (Just (x::xs), Selected rule) ->
                    let proofArray = Array.fromList (List.map .proof model.proofs)
                    in
                    case Array.get x proofArray of
                        Just proof ->
                            case updateProof xs model (model.system.rule rule) proof of
                                Err errMsg ->
                                    { model | messages = model.messages ++ [errMsg] }

                                Ok newProof ->
                                    { model | proofs =
                                        List.map (\val -> {proof = val})
                                            <| Array.toList <| Array.set x (Debug.log "newProof" newProof) proofArray }

                        Nothing ->
                            { model | messages = model.messages ++ ["invalid path to node"]}

                (Nothing, Selected rule) ->
                    let
                        infRule = model.system.rule rule
                        sub = model.system.substitute Dict.empty
                        resultProof = Proof.instantiateRule sub infRule
                    in
                    case (infRule, resultProof) of
                        ({conditions, conclusion}, Ok proof)  ->
                            { model
                                | selection = NoSelection
                                , proofs = model.proofs ++ [{proof = proof}]
                            }

                        _ ->
                            model

                _ ->
                    model

        EnterNode nodePath ->
            { model | overNode = Just nodePath }

        ExitNode nodePath ->
            case model.overNode of
                Just path ->
                    if path == nodePath
                    then { model | overNode = Nothing }
                    else model

                Nothing ->
                    model

        UpdateFromBox str ->
            { model | fromBox = str }

        UpdateToBox str ->
            { model | toBox = str }

        RenameProps ->
            let
                parsedFrom = model.system.readProp model.fromBox
                parsedTo = Maybe.map model.system.makeFormula <| model.system.readProp model.toBox
            in
            case (parsedFrom, parsedTo) of
                (Just from, Just to) ->
                    { model | ruleSubs = Dict.insert model.system.printProp from to model.ruleSubs }

                _ ->
                    { model | fromBox = "", toBox = "", messages = model.messages ++ ["failed"] }